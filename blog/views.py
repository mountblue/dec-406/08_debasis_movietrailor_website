from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView, DetailView, CreateView, UpdateView, DeleteView )
from .models import Movie
# Create your views here.


def home(request):
    context = {
        "movies":Movie.objects.all()
    }
    return render(request, "blog/home.html", context)

class MovieListView(ListView):
    model = Movie
    template_name = "blog/home.html"
    context_object_name = "movies"
    ordering = ["-created_post_date"]
    paginate_by = 2

class UserMovieListView(ListView):
    model = Movie
    template_name = "blog/user_movies.html"
    context_object_name = "movies"
    paginate_by = 2

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get("username"))
        return Movie.objects.filter(submitted_by=user).order_by("-created_post_date")


class MovieDetailView(DetailView):
    model = Movie

class MovieCreateView(LoginRequiredMixin,CreateView):
    model = Movie
    fields = ["title", "director","date_of_release","genre","src"] 

    def form_valid(self, form):
        form.instance.submitted_by = self.request.user
        return super().form_valid(form)

class MovieUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Movie
    fields = ["title", "director","date_of_release","genre","src"] 

    def form_valid(self, form):
        form.instance.submitted_by = self.request.user
        return super().form_valid(form)

    def test_func(self):
        movie = self.get_object()
        if self.request.user == movie.submitted_by:
            return True
        return False

class MovieDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Movie
    success_url = "/"

    def test_func(self):
        movie = self.get_object()
        if self.request.user == movie.submitted_by:
            return True
        return False


def about(request):
    return render(request, "blog/about.html", {"title":"about"})

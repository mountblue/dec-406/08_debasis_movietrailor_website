from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class Movie(models.Model):
    title = models.CharField(max_length=100)
    director = models.CharField(max_length=100)
    date_of_release = models.DateField()
    created_post_date = models.DateTimeField(default=timezone.now)
    genre = models.CharField(max_length=50)
    src = models.CharField(max_length=100)
    submitted_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("movie-detail", kwargs={"pk": self.pk})
    

